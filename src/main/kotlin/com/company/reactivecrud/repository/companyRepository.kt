package com.company.reactivecrud.repository

import com.company.reactivecrud.Company
import org.springframework.data.mongodb.repository.ReactiveMongoRepository

interface companyRepository : ReactiveMongoRepository<Company, String>