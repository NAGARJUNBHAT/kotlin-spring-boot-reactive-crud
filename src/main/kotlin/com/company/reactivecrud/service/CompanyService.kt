package com.company.reactivecrud.service

import com.company.reactivecrud.Company
import com.company.reactivecrud.Employee
import com.company.reactivecrud.exception.NotFoundException
import com.company.reactivecrud.repository.EmployeeRepository
import com.company.reactivecrud.repository.companyRepository
import com.company.reactivecrud.request.CompanyRequest
import org.springframework.data.crossstore.ChangeSetPersister
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty

@Service
class CompanyService(
    private val companyRepository: companyRepository,
    private val employeeRepository: EmployeeRepository
) {
    fun createCompany(request: CompanyRequest): Mono<Company> =
        companyRepository.save(
            Company(
                name = request.name,
                address = request.address
            )
        )

    fun  findAll(): Flux<Company> =
        companyRepository.findAll()

    fun findById(id: String): Mono<Company> =
        companyRepository.findById(id).switchIfEmpty { Mono.error(NotFoundException("Company with id $id not found")) }

    fun updateCompany(id: String, request: CompanyRequest): Mono<Company> =
        findById(id).flatMap { companyRepository.save(it.apply {
            name = request.name
            address = request.address
        }) } .doOnSuccess { updateCompanyEmployees(it).subscribe() }

    fun deleteById(id: String): Mono<Void> =
        findById(id).flatMap(companyRepository::delete)

    private fun updateCompanyEmployees(updatedCompany: Company): Flux<Employee> =
        employeeRepository.saveAll(
            employeeRepository.findByCompanyId(updatedCompany.id!!).map { it.apply { company = updatedCompany } }
        )
}