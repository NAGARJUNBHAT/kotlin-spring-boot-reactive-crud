package com.company.reactivecrud

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ReactivecrudApplication

fun main(args: Array<String>) {
	runApplication<ReactivecrudApplication>(*args)
}
